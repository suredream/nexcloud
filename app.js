var express = require('express');
var path = require('path');
var app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(express.static('public'))

app.get('/', function (req, res, next) {
  // res.send('Hello World!');
  res.render('index', { title: 'NEXCloud' });
});

app.get('/getstarted', function (req, res, next) {
  // res.send('Hello World!');
  res.render('getstarted', { title: 'Get Started' });
});

app.get('/account', function (req, res, next) {
  // res.send('Hello World!');
  res.render('account', { title: 'AWS Account' });
});

app.get('/datasets', function (req, res, next) {
  // res.send('Hello World!');
  res.render('datasets', { title: 'NEX Datasets' });
});

app.get('/repos', function (req, res, next) {
  // res.send('Hello World!');
  res.render('repos', { title: 'NEX Repositories' });
});


app.get('/tasks', function (req, res, next) {
  // res.send('Hello World!');
  res.render('tasks', { title: 'Cloud Tasks' });
});

app.get('/create-instance', function (req, res, next) {
  // res.send('Hello World!');
  // res.render('tasks', { title: 'NEXCloud' });
	console.log('create-instance successful')
	var exec = require('child_process').exec;
	var cmd = 'aws ec2 run-instances --image-id ami-840f60fc --count 1 --instance-type t2.micro --key-name jun-eb2key --subnet-id subnet-a7fe35de --security-group-ids sg-74294b0a --user-data file://my-script.sh';

	exec(cmd, function(err, stdout, stderr) {
	  if (err) {
	    // node couldn't execute the command
	    return;
	  }
	  // the *entire* stdout and stderr (buffered)
	  console.log(`stdout: ${stdout}`);
	  console.log(`stderr: ${stderr}`);
	});
	next()
});

var port = process.env.PORT || 3000;
// var port = 3000;
app.listen(port, function() {
  console.log('NEXCloud listening on port at %s', port);
});
