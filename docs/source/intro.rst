In this user manual, we show you how to use the climate access tools to create and use your own datasets. These tools were produced by Planet OS in collaboration with the NASA Earth Exchange (NEX) team with the intent of improving data access to OpenNEX datasets.

OpenNEX contains a huge reserve of Coupled Model Intercomparison Project Phase 5 (CMIP5) climate modelling data that is freely available to all researchers. This includes DCP30 model runs for the Continental United States and GDDP model runs for the full globe.

The climate access tool makes it easy to select the data of interest to you and create a custom data product from that data. The climate access tool requires no programming and produces CSV or NetCDF files that can be used for climate science work done in languages such as Python, R, Matlab, and even Excel.

Please note that the climate access tools have been released as a pilot version. That means that it has a very preliminary set of capabilities and features. We plan to refine and extend it based on what users (such as yourself) find useful and want. Please share any feedback you have with us by sending an email to feedback@planetos.com.