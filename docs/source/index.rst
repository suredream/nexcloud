.. nexcloud documentation master file, created by
   sphinx-quickstart on Tue May  1 09:59:50 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to nexcloud's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
