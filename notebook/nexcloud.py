print 'NEXCloud version 0.4.4 2018-5-10 -- ""'

from collections import namedtuple
Scan = namedtuple('scan' , 'year day hr minute')


def georeference(scan):
    print 'georeference module: dry-run'
    pass


def maiac(scan):
    print 'maiac module: dry-run'
    pass



def init(workdir):
	pass

def download_ABI_from_AWS_S3_bucket(scan, type):

    s3_noaa_goes16 = 's3://noaa-goes16/ABI-L1b-RadF/'
    source = 's3://noaa-goes16/ABI-L1b-RadF/{0.year:4d}/{0.day:03d}/{0.hr:02d}/'.format(scan)
    dest = 'local-fdisk/'
    option = '--recursive --exclude "*" --include *_G16_s{0.year:4d}{0.day:03d}{0.hr:02d}{0.minute:02d}*'.format(scan)
    awscmd = ' '.join(['aws s3 cp', source, dest, option])
    print awscmd
    # os.system(awscmd)
    print 'starting to download......'
    return [] #sorted(glob.glob(app['temp'] + '/*_G16_s{0.year:4d}{0.day:03d}{0.hr:02d}{0.mi:02d}*'.format(scan)